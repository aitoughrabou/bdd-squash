Feature: connexion application Jpetstore

Scenario: connexion
   Given un navigateur est ouvert
   When je suis sur url
   And je clique sur le lien de connexion
   And je rentre le username "j2ee"
   And je rentre le password "j2ee"
   And je clique sur login
   Then utilisateur ABC est connecte
   And je peux lire le message acceuil "Welcome ABC!"
   And je clique sur logout et ferme le navigateur