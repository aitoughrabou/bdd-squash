Feature: connexion application Jpetstore

Scenario Outline: connexion
   Given un navigateur est ouvert
   When je suis sur url
   And je clique sur le lien de connexion
   And je rentre le username "<username>"
   And je rentre le password "<password>"
   And je clique sur login
   Then utilisateur ABC est connecte
   And je peux lire le message acceuil "<welcomeMsg>"
   And je clique sur logout et ferme le navigateur
   
Examples:
	| username | password |  welcomeMsg  |
	| j2ee     | j2ee     | Welcome ABC! |
	| ACID     | ACID     | Welcome ABC! |