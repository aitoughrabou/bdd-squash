package gherkinJava;

import org.openqa.selenium.WebDriver;
import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HealthCare {
	
	
	WebDriver driver;
	
	
	@Given("je suis connecte sur la page de prise de rendez vous")
	public void je_suis_connect_sur_la_page_de_prise_de_rendez_vous() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
	    driver.get("https://katalon-demo-cura.herokuapp.com/");
	    
	    driver.findElement(By.id("menu-toggle")).click();
	    driver.findElement(By.xpath("//a[@href='profile.php#login']")).click();
	    
	    driver.findElement(By.xpath("//input[@name='password']")).clear();
	    driver.findElement(By.xpath("//input[@name='username']")).sendKeys("John Doe");
		driver.findElement(By.xpath("//input[@name='password']")).clear();
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("ThisIsNotAPassword"); 
		
		driver.findElement(By.id("btn-login")).click();
	}

	@When("je renseigne les informations obligatoires")
	public void je_renseigne_les_informations_obligatoires() {
		driver.findElement(By.id("txt_visit_date")).sendKeys("14/06/2022");
		driver.findElement(By.id("txt_comment")).sendKeys("comments");
	}

	@When("je clique sur book appointment")
	public void je_clique_sur_book_appointment() {
		driver.findElement(By.id("btn-book-appointment")).click();
	}

	@Then("le rendez vous est confirme")
	public void le_rendez_vous_est_confirme() {
		assertEquals("Please be informed that your appointment has been booked as following:", driver.findElement(By.xpath("//p[@class='lead']")).getText());
		driver.close();;
	}


	

}
