package gherkinJava;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class JPetStore {
	
	
	
	WebDriver driver;
	
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.gecko.driver", "./rsc/geckodriver.exe");
		driver = new FirefoxDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);	
	    driver.get("https://petstore.octoperf.com/actions/Catalog.action");
	}

	@When("je suis sur url")
	public void je_suis_sur_url() {
		assertEquals("JPetStore Demo", driver.getTitle());
	}

	@When("je clique sur le lien de connexion")
	public void je_clique_sur_le_lien_de_connexion() {
		driver.findElement(By.xpath("//a[text()='Sign In']")).click();
	}

	@When("je rentre le username {string}")
	public void je_rentre_le_username(String username) {
		driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);
	}

	@When("je rentre le password {string}")
	public void je_rentre_le_password(String username) {
		driver.findElement(By.xpath("//input[@name='password']")).clear();
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys(username);
	}

	@When("je clique sur login")
	public void je_clique_sur_login() {
		driver.findElement(By.xpath("//input[@name='signon']")).click();
	}

	@Then("utilisateur ABC est connecte")
	public void utilisateur_ABC_est_connecte() {
		driver.findElement(By.id("WelcomeContent")).isDisplayed();
	}

	@Then("je peux lire le message acceuil {string}")
	public void je_peux_lire_le_message_acceuil(String text) {
		assertEquals(text, driver.findElement(By.id("WelcomeContent")).getText());
	}
	
	@When("je clique sur logout et ferme le navigateur")
	public void je_clique_sur_logout() {
		driver.findElement(By.xpath("//a[text()='Sign Out']")).click();
		driver.close();
	}

}
